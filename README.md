# UTroot

[JSROOT](https://github.com/root-project/jsroot) packaged for [Ubuntu Touch](https://ubuntu-touch.io/)

JSROOT credits to original developers

## License

Copyright (C) 2021  Francesco Giuseppe Gravili

Licensed under the MIT license
